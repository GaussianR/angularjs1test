var gulp = require('gulp'),
    jade = require('gulp-jade'),
    sass = require('gulp-sass'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    browserify = require('gulp-browserify');

var build_folder = gutil.env.production ? 'production' : 'development';

//Compiles all JADE templates
gulp.task('jade', function () 
{
  return gulp.src('src/templates/**/*.jade')
          .pipe(jade())
          .pipe(gulp.dest('builds/' + build_folder));
});

//Compiles JS and applies all post-compile operations.
gulp.task('js', function()
{
  return gulp.src('src/js/**/*.js')
          .pipe(browserify({debug: gutil.env.production}))
          .pipe(gutil.env.production ? uglify({mangle: true}) : gutil.noop())
          .pipe(gulp.dest('builds/' + build_folder + '/assets/js'));
});

gulp.task('sass', function()
{
  var config = 
  {
    outputStyle: gutil.env.production ? 'compressed' : 'expanded',
    sourceComments: gutil.env.production ? false : 'map'
  };
  
  return gulp.src('src/sass/**/*.scss')
          .pipe(sass(config))
          .pipe(gulp.dest('builds/' + build_folder + '/assets/css'));
});

//Watch changes on files
gulp.task('watch', ['jade', 'js', 'sass'], function() 
{
  gulp.watch('src/js/**/*.js', ['js']);
  gulp.watch('src/sass/**/*.scss', ['sass']);
  gulp.watch('src/templates/**/*.jade', ['jade']);
});

gulp.task('default', ['jade', 'js', 'sass']);
gulp.task('dev', ['watch']);