var $ = require('jquery');

$(function() 
{
  var angular = require('angular');
  var app = angular.module('MainApp', []);
  app.controller('MainController', function($scope)
  {
    $scope.getPasswordStrength = function(password)
    {
      if(/^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])(?=\S*?[^\w\*])\S{5,}$/.test(password))
        return 4; //Very strong password
      else if(/^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])\S{5,}$/.test(password))
        return 3; //Strong password
      else if(/^(?=\S*?[a-z])(?=\S*?[0-9])\S{5,}$/.test(password))
        return 2; //Medium password
      else if(/(?=.{5,}).*/.test(password))
        return 1; //Weak password

      return 0;
    };
    
    $scope.validation = {
      username: {
        showError: function()
        {
          var context = $scope.register.username;
          return context.$dirty && context.$invalid && context.$touched;
        },
        errorMessage: function()
        {
          return 'Username is required.';
        }
      },
      email: {
        showError: function()
        {
          var context = $scope.register.email;
          return context.$dirty && context.$invalid && context.$touched;
        },
        errorMessage: function()
        {
          var context = $scope.register.email;
          if(context.$error.required)
            return 'Email is required.';
          else
            return 'Invalid email address.';
        }
      },
      password: {
        showError: function()
        {
          var context = $scope.register.password;
          return context.$dirty && context.$invalid && context.$touched;
        },
        errorMessage: function()
        {
          var context = $scope.register.password;
          if(context.$error.required)
            return 'Password is required.';
          else if(context.$error.password)
            return 'Password is so weak.';
        }
      },
      passwordc: {
        showError: function()
        {
          var context = $scope.register.passwordc;
          return context.$dirty && context.$invalid && context.$touched;
        },
        errorMessage: function()
        {
          var context = $scope.register.passwordc;
          if(context.$error.required)
            return 'Password is required.';
          else if(context.$error.passwordmatch)
            return 'Passwords don\'t match';
        }
      },
      submit: {
        disable: function()
        {
          return $scope.register.$invalid ||
                 $scope.validation.username.showError() ||
                 $scope.validation.email.showError() ||
                 $scope.validation.password.showError() ||
                 $scope.validation.passwordc.showError();
        }
      }
    };
  });
  
  app.directive('passwordCheck', function()
  {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, controller)
      {
        controller.$parsers.push(function(value)
        {
          controller.$setValidity('password', scope.getPasswordStrength(value) > 1);
          return value;
        });
      }
    };
  });
  
  app.directive('passwordMatch', function()
  {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, controller)
      {
        scope.$watchGroup([attr.ngModel, attr.passwordMatch], function (value) 
        {
          controller.$setValidity('passwordmatch', value[0] === value[1]);
        });
      }
    };
  });
});

