var $ = require('jquery');

$(function() 
{
  var angular = require('angular');
  require('angular-i18n/angular-locale_en-gb');
  
  var app = angular.module('MainApp', []);
  app.controller('MainController', function($http, $scope, $interval, $location)
  {
    $scope.author = {
      name: 'Mario',
      phone: '+34 111222333',
      email: 'anymail@randomail.com',
      country: 'Spain',
      lastname: 'Salazar de Torres'
    };
    
    $scope.cars = {
      car01: {brand: "Ford", model: "Mustang", color: "red"},
      car02: {brand: "Fiat", model: "500", color: "white"},
      car03: {brand: "Volvo", model: "XC90", color: "black"}
    };

    
    $http.get("http://www.w3schools.com/angular/customers.php")
            .then(function (response) 
    {
      $scope.members = response.data.records;
    });
    
    $scope.now = Date.now();
    $interval(function()
    {
      $scope.now = Date.now();
    }, 500);
    
    $scope.url = $location.absUrl();
  });
  
  app.directive('profile', function()
  {
    return {
      restrict: 'E',
      scope: {
        user: '=person'
      },
      template: '<p><label>Name: </label>{{user.name}}</p>\n' +
                '<p><label>Lastname: </label>{{user.lastname}}</p>\n' +
                '<p><label>Country: </label>{{user.country}}</p>\n' +
                '<p><label>Phone: </label>{{user.phone}}</p>\n' +
                '<p><label>Email: </label>{{user.email}}</p>\n'
    };
  });

  app.filter('capitalize', function ()
  {
    return function (x)
    {
      return x.toLowerCase().replace(/\b\w/g, function (m)
      {
        return m.toUpperCase();
      });
    };
  });
});